#!/usr/bin/env python


#Example of low level hardware PWM 
#You will notice how smooth this method is when compared to the software implementation
 
import time
 
import pigpio
 
servos = 18 #GPIO number
pi = pigpio.pi() 
#pigpio.start()
#pulsewidth can only set between 500-2500

#This configuration is for the SM-S23095 Servo found in the Arduino Starter Kits

try:
    while True:
        pi.set_servo_pulsewidth(servos, 575) #0 degree
        print("Servo {} {} micro pulses 0 Deg".format(servos, 575))
        time.sleep(1)
        pi.set_servo_pulsewidth(servos, 1500) #90 degree
        print("Servo {} {} micro pulses 90 Deg Centre".format(servos, 1500))
        time.sleep(1)
        pi.set_servo_pulsewidth(servos, 2350) #180 degree
        print("Servo {} {} micro pulses 180 Deg".format(servos, 2350))
        time.sleep(1)
        pi.set_servo_pulsewidth(servos, 1500)
        print("Servo {} {} micro pulses".format(servos, 1500))
        time.sleep(1)
 
   # switch all servos off
except KeyboardInterrupt:
    pi.set_servo_pulsewidth(servos, 0);
 
pi.stop()

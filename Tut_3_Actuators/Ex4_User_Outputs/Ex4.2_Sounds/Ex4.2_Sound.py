# This Example shows how to use the mpg321 media player
# This is a bash command line tool, so we will need to use the os.system() function

#Example 4.2

import os     # since system() is a function of the os module, we need to import it
os.system("nohup mpg321 sailing_by.mp3 > /dev/null 2>&1 &") # This runs in the background

# You can use any mp3 to add sound to any project
# Just prepare your sound effects using a free tool such as Audacity
# Use WINSCP or a Linux SCP tool to copy mp3 audio files to your Raspberry Pi



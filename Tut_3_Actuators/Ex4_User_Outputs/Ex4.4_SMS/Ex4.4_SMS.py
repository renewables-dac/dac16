#Example 4.4 Sending an SMS

import os
API_hash="9d89b5349669ccad2757de29a38a65bc" # If you sign up for your own account, replace this with your API hash

phone_number=raw_input("Phone number to send to :")#Clearly, the number to send to can be set to whatever you like without user input
text_message=str(raw_input("Message to send :"))   # SMS alert text could be generated by your system in specific events

os_command=("curl --data \"to="+str(phone_number)+"&message="+text_message+"&hash="+API_hash+"\" http://www.smspi.co.uk/api/send/")
print os_command
os.system(os_command)





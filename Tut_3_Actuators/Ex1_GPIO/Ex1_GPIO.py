# Example of setting up GPIO pin as an output
# This is the equivalent of blink for the Arduino!
# See tutorial 3.1 wiring diagram


import os
import RPi.GPIO as GPIO	#import GPIO module
import time
GPIO.cleanup()
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)      #BCM uses Broadcom pin numbering
GPIO.setup(17,GPIO.OUT)     #define pin 17 as output pin


while 1:
    GPIO.output(17,GPIO.HIGH)   # Set pin 17 High, (1 or True) to 3.3V
    print ("GPIO is HIGH")
    time.sleep(4)
    os.system("clear")		#clears the terminal window


    GPIO.output(17,GPIO.LOW)    # Set pin 17 Low, (0 or False) to 0V
    print("GPIO is LOW")
    time.sleep(4)	
    os.system("clear")		#Press CTRL+C to break out of the loop

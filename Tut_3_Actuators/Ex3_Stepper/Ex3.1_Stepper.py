import RPi.GPIO as GPIO
import time
     
GPIO.setmode(GPIO.BCM)

coil_A_1_pin = 18
coil_A_2_pin = 23
coil_B_1_pin = 24
coil_B_2_pin = 25

GPIO.setup(coil_A_1_pin, GPIO.OUT)
GPIO.setup(coil_A_2_pin, GPIO.OUT)
GPIO.setup(coil_B_1_pin, GPIO.OUT)
GPIO.setup(coil_B_2_pin, GPIO.OUT)

def forward(delay, steps):  
    for i in range(0, steps):
        setStep(1, 0, 1, 0)
        time.sleep(delay)
        setStep(0, 1, 1, 0)
        time.sleep(delay)
        setStep(0, 1, 0, 1)
        time.sleep(delay)
        setStep(1, 0, 0, 1)
        time.sleep(delay)
     
def backwards(delay, steps):  
     for i in range(0, steps):
         setStep(1, 0, 0, 1)
         time.sleep(delay)
         setStep(0, 1, 0, 1)
         time.sleep(delay)
         setStep(0, 1, 1, 0)
         time.sleep(delay)
         setStep(1, 0, 1, 0)
         time.sleep(delay)

def setStep(w1, w2, w3, w4):
    GPIO.output(coil_A_1_pin, w1)
    GPIO.output(coil_A_2_pin, w2)
    GPIO.output(coil_B_1_pin, w3)
    GPIO.output(coil_B_2_pin, w4)


def stepper_off():
    GPIO.output(coil_A_1_pin, False)
    GPIO.output(coil_A_2_pin, False)
    GPIO.output(coil_B_1_pin, False)
    GPIO.output(coil_B_2_pin, False)


while True:
    print "At rest coils off to prevent overheat\n"
    stepper_off()
    GPIO.output
    delay = raw_input("Delay between steps (milliseconds)?")
    steps = raw_input("How many electrical cycles forward? ")
    forward(int(delay) / 1000.0, int(steps))
    print "At rest coils off to prevent overheating\n"
    stepper_off()
    steps = raw_input("How many electrical cycles backwards? ")
    backwards(int(delay) / 1000.0, int(steps))

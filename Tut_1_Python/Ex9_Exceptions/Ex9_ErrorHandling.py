##### Example 9 Error Handling ######

# Sometimes Python won't be able to successfully finish a job you ask it to do. 
# If a sensor goes bad, or the internet connection momentarily breaks, or there is a bad packet of data which turns out to be gibberish...
# We don't want these small problems to completely stop our system from funtioning by
# throwing the code out of correctly running due to errors.

# Here we will look at error handling using the try: except: pairs to help keep your 
# code flowing smoothly and your system up for longer

#############################################################################

#Ex 9.1
print ("Ex 9.1") # A very blunt instrument for handling errors is
                 # the universal try: except: pair

try:
   badvar=(238293.2398 + "Amber Rudd") # Mixing strings and numbers without explicitly 
                                       # converting the number to a string will usually
                                       # throw an error
   print(badvar)

except:         # NOTE THE TRY: EXCPEPT: pair are both used in the same
                # indentation level unlike while: or def function(arg1,arg2,argn): statements
   print ("There was an error")
   print ("but I don't care about remedying it by finding the cause of the error")
   print ("only that my system doesn't crash")

#Ex 9.2
print("\nEx 9.2")

try:
    import nosuchmodule
except ImportError:
    print ("\nThere was an import error... but instead of crashing your code, I will still try to run ")
    print("Unlikley if the module was later used to call functions and variables")
   

#Example 9.3
print ("\nEx 9.3")
#variable
try:
    print nosuchmodule.nosuchvariable
except NameError:
    print ("Name Error - variable not defined\nCheck your bleddy code...")
except:
    print("there was a problem, but we didn't expect this type....")#always include a catch all if you dont want your script to terminate unexpectedly



# Your error handlers can include lots of options to catch different errors, 
# logical operators, etc



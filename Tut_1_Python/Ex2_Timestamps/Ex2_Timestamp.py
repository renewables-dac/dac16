#EXAMPLE 2
#Example of python time formatting

#Required module: time
import time

#raw time is handled as second since the "Epoch"... not very useful for us humans
#Ex 2.1
print ("\nExample 2.1")
now=(time.time())
print (str(now) + " seconds since 0:00:00 Jan 01 1970")


#Ex 2.2
print("\nExample 2.2")
#time.ctime() returns a string in a muh more human readable format...
print ("Human readable Time is now ")
print (time.ctime())

#Line spacing - special characters \n introduce a line space in printed output.
print("********")
print ("\n")
print("********")

#Ex 2.3
print ("\nExample 2.3")
#time.strftime("   ") % symbol and date / time element identifier used to construct a custom timestamp. Many to choose from!
#For construction of XML data points, for insertion in MySQL database use the following format to yield YYYY-MM-DD hh:mm  
timestamp = (time.strftime("%Y-%m-%d %H:%M"))
#String concatenation example
print ("The time formatted as compatible with MySQL for database insertion is "+ timestamp)

########################### Formatting options ########################
#See http://strftime.net/ for a very useful web based timestamp 
#formatting tool which will produce the argument for your strftime() function
# strftime("COPY_AND_PASTE_argument_HERE_FROM_WEBPAGE_DIALOG")
#No need to memorise or look up %h%d etc etc.... Just use the tool.

#Here's a silly example I just tried using the online tool:
#Ex 2.4
print ("\nExample 2.5")
print (time.strftime("There have been %j days so far in %Y and the time is now %M minutes past %l%p."))


########################## KEY POINTs: #############
#for scripts requiring a timestamp

#1) import the time module
#2) assign a timestamp using the strftime MySQL friendly format as prescribed

#######################################################

#Tutorial 3 - 
#XML for data capture

#To construct XML, we will need to be able to handle a mixture of numerical values and text.
#Remember that variables can be strings, integers, floating point numbers, boolean, etc.
#Also, remember that Python cannot join variables of different types unless we make our intentions explicitly clear:

#A simple string of XML:
#XML is a way of storing data with a self describing hierarchy, and is therefore more useful than comma separation (CSV)
#It is very useful for importing data to a spreadsheet or to a database table


#Example 3.1
print ("Ex3.1")
#The <Data_point> tag has a </Data_point> closing tag pair.
xml_1="<Data_point></Data_point>"
print (xml_1)


#Example 3.2
print ("Ex3.2")
#Let's create a timestamp in MySQL friendly format
import time
timestamp=time.strftime("%Y-%m-%d %H:%M")
#The Time_stamp tag is a child tag of Data_point
xml_2="<Data_point><Time_stamp>"+timestamp+"</Time_stamp></Data_point>"
print(xml_2)
#We can concatenate the timestamp directly with the rest of the string, since strftime returns a string anyway...

#Example 3.3
print("Ex3.3")
#What about inserting a numerical value representing some variable we have recorded or computed?
a=float(243)
b=float(13)
value1=a/b
xml_3="<Data_point><Time_stamp>"+timestamp+"</Time_stamp><Value>"+str(value1)+"</Value></Data_point>"
print xml_3
#Note the use of str(value) to convert the floating point variable to a string.
#If you don't do this, Python will throw an error. Try removing the str() and re run to see it
#Incidentally, the Value tag is a SIBLING tag to Time_Stamp. Data_Point is the PARENT tag of both Time_stamp and Value.


##############   Key points for Construction of XML strings for data records #####################################################

#1)Decide on an identifying wrapper tag for each unique data point
#2)Try to include a unique variable, could be timestamp, displacent, (x,y,z position), sample number etc
#3)Group tags in a logical manner using parent / child / sibling relationships for easy navigation later (particulary for EXCEL)
#4)Remember to convert all variables to strings using the str() function before concatenation
#5)Close your tags using </the_same_name> and beware of inconsistency in capitalisation or spelling

###################################################################################################################################


############################ Database Hygiene ####################################### 
#If you progress to importing your lines of XML to a database, the rows will need to be identified by a parent tag. 
#In the example above, Data_Point would be a suitable identifier to seperate records in the database table.
#Time_stamp would be a suitable primary key
##########################################################






###### Modules like this are really useful for configuring your sensor systems ########


# for example, you can store the hardware IDs for all of your DS18B20 temperature probes
# This allows dynamic referencing - in case a probe goes bad to avoid recoding just tweak the config file!

Probe_ID_1 = "00000fe339290d"
Probe_ID_2 = "00000fd2199028"


#########################################################################


#Also useful for storing calibration data such as plot slopes and intercepts for sensor instruments

PT100_slope=243.339002483
PT100_intercept=44184.473893

#Example 12 - ADXL345 Accelerometer
# 
# Our first example of actuation driven by sensing and logic using the Pi
# This is essentially how the slurry tank cover pump is triggered
# Hook up an LED on GPIO 24 with a resistor to protect it to GND
import os
from adxl345 import ADXL345

import RPi.GPIO as GPIO
import time
GPIO.cleanup()
GPIO.setwarnings(False)
wait=2
GPIO.setmode(GPIO.BCM)
GPIO.setup(24,GPIO.OUT)

GPIO.output(24,GPIO.HIGH)
time.sleep(wait)
GPIO.output(24,GPIO.LOW)
time.sleep(wait)

while 1:  
	adxl345 = ADXL345()
	axes = adxl345.getAxes(True)
	os.system("clear")
	print "ADXL345 on address 0x%x:" % (adxl345.address)
	print "   x = %.3fG" % ( axes['x'] )
	print "   y = %.3fG" % ( axes['y'] )
	print "   z = %.3fG" % ( axes['z'] )
	xg=float(axes['x'])
	if xg > 0.16:
		GPIO.output(24,GPIO.HIGH)
	if xg< 0.11:
		GPIO.output(24,GPIO.LOW)



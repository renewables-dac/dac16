#Example 6.3 - Capture serial input from Arduino and strip out the parts we want by finding key word positions 


import serial

ser = serial.Serial('/dev/ttyACM0', 9600) # Set the ports and baud rate 9600 baud = 9600 bits per second ~9.6kB/s
ser.close()
ser.open()


dump="12345"
for x in dump:
   serial_dump=ser.readline()             #dump out first 5 messages (could be incomplete and throw errors if we try to manipulate the string)



while 1:
    serial_string=ser.readline()
    print(serial_string)               # Of course, if we are building the Arduino system we can choose to just send the data value without any other garbage
                                       # But if we are sending more than one sensor output, it is likely you will need to strip out values by detecting key words
                                       # or by counting character spaces

                                                   # In this example, we will count forwards from the "degrees c" position, as the positions
                                                   # could change with more characters in the numbers at higher values                                                   
                                                   # We know that the string is always in the form:
                                                   # sensor Value: 133, Volts: 0.65, degrees C: 14.94
                                                   # The temperature value begins 2 charaters after 'degrees C:', which is 11 characters after the d
                                                   # The temperature value is 5 characters long, so ends 16 charcaters after the d

    
    degCpos=serial_string.find("degrees C:")               #returns start character position of the phrase "degrees C:"
    raw_temp=(str(serial_string[degCpos+11:degCpos+16]))   # strips out the part of the string we want, counting forwards from the 1st character of search phrase
    print(raw_temp)

    rawtempx10=(float(raw_temp)*10)                        # Once numerical values are stripped, it's a good idea to convert them to a floating point variable
    print("10x Temp value = "+str(rawtempx10))             # Now we can use the stripped out value for calculations or string concatenation


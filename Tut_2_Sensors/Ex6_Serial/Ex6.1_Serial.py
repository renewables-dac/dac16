#Example 6.1 - Display serial input from Arduino 


import serial
ser = serial.Serial('/dev/ttyACM0', 9600) #Set the ports and baud rate 9600 baud = 9600 bits per second ~9.6kB/s


while 1:
    serial_string=ser.readline()
    print(serial_string)    
